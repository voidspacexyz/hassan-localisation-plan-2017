## Hassan Localisation Plan

### (Still hugely WIP. We should have updated everything with a extremely clear plan by 25th Oct 2017)

## Team : (Feel free to add names if I have missed)
  ####  Core Team :

        - voidspacexyz and Darshan from FSMK
        - Amit, Chinmayee, Danial, Sumanth from GEC
        - Ashok, Kushal, Harshit, Subha from RIT

  #### Media Team :

    - Madhushree and voidspacexyz

  ####  Resource People :

    - Darshan (OSM)

    - Rizma   (Zanata)

    - Madhushree (Wikipedia)

    - voidspacexyz (General systems setup and related activites)

---

## Overview:
- Atleast 6 pre-events before Nov 1:
    - 3 in RIT and GEC glugs
    - 3 public events, one in a public place, two in schools.
    - Any activity that happens beyond is a bonus


- A Grand Localisation gathering on Nov 1st, inviting all the people from hassan, who have come across all these 6 events to have a very large gathering.


## List of Activities:
#### Public Activities:


-  **Big Bazzar**  - Bag of Words with gift vouchers .

    - **Description** : We will be gathering a huge list of words in english that are not there in kannada(across wikipedia, softwares etc) and writing down cards for each word. Each card can have upto a maximum of 4 synonym kannada words. The gift vouchers will be given based on the number of words someone translates , possibly in a given time or based on someother mechanism.

    - **Status** : Not Happening. We decided not to do it here, since we were not greately prepared to handle it in public yet. 

    - **Point Of Contact** : Darshan, Amit, Aslam, Sumanth

    - **Comments** : Target time - Saturday/Sunday evenings/


- **Bus Stand / Park**  - Flash Mob with Bag of Words

  - **Description** : These places are too filled with all sorts of random people , same as big bazzar , and we cant expect people to be literates. The plan is to identify the time, when most of the crowd will gather at this place, and organize a flash mob to gather audience at the booth. Once people gather at the booth, the bag of words continue.

  - **Status** : The same as Big Bazzar.

  - **Point Of Contact** : Ashok, voidspacexyz.

  - **Comments** : The volunteers needs to be extremely prepared to handle places like this, else it would be a bad hit on us.

#### School Activities

- **CKS School (CKS)**

    - **Description** : The primary target here is 9 - 10 students and possibly 1st year PU. There is still no clear plan as to what could possibly be the right way to do things here. Bag of words kinda activities might not interest students. We need to implement / think of a better way of doing stuff.

    - **Status** : 30th Oct 2017 Morning 10 AM - 12 AM.

    - **Point of Contact** : Subha, Kushal.

    - **Comments** : Students from RIT and GEC who have been doing translations are taking care of this. 


- **Hassan Public School (HPS)**

  - **Description** : The primary target here is 9 - 10 students and possibly 1st year PU. There is still no clear plan as to what could possibly be the right way to do things here. Bag of words kinda activities might not interest students. We need to implement / think of a better way of doing stuff.

  - **Status** : We are doing this school after Nov 7.

  - **Point of Contact** : Chinmayee and Danial

  - **Comments** : The school had already planned a lot of things and we were sure we would not be able to grab enough attention. So we decided to let the school settle down and later apprach them.

#### GLUG Activities

    GLUG's could take up translating gnu.org and other FOSS related articles, since the understanding of the concept here would be great. Once the GLUG people are trained up and have translated atleast one article, during the public event, the general audience could be tied up with some of the GLUG volunteers who were involved in translating, so there would be a great interaction between them and the concept of FOSS itself could be propogated better. In a way, we are targetting to get the GLUGers to interact with the common audience on FOSS.

- **GEON GEC**

  - **Description** : The GLUG students to be given articles that are listed in zanata and translate them. These articles primarily explain various concepts of Free Society .

  - **Status** : Finished on Oct 27. About 4 -5 articles were translated on paper, these would be updated on Zanata.

  - **Point of Contact** : Chinmayee and Danial

  - **Comments** : 


- **GLUE GEC**

  - **Description** : The GLUG students to be given articles that are listed in zanata and translate them. These articles primarily explain various concepts of Free Society .

  - **Status** : Oct 31 2017 in GLUG session

  - **Point of Contact** : Amit and Sumanth

  - **Comments** : 


- **RIT GLUG**

  - **Description** : The GLUG students to be given articles that are listed in zanata and translate them. These articles primarily explain various concepts of Free Society .

  - **Status** : - **GEON GEC**

  - **Description** : The GLUG students to be given articles that are listed in zanata and translate them. These articles primarily explain various concepts of Free Society .

  - **Status** : Oct 30th Post Lunch 

  - **Point of Contact** : Subha, Kushal, Ashok.

  - **Comments** : 

    
- **Expectations** :

    - GLUG people understand what FOSS it and how the whole thing works.

    - GLUG people need not have translated, but even a clear understanding on how translations to be done and the ability to interact with the people is an important skill.

    - GLUG people understand the following topics at the minimum: (Reading Section below has links for reading up on the same)

    - What is FOSS and its need.

    - The common myths of FOSS and how to answer them

    - Understanding of translation tool that you are interested in ( Refer Translations Planned section below for the same)

    - Understanding what is localisation and why it is important.

    - Understanding of fonts and keyboards setup  on computers

    - Understanding of licenses of fonts, tools and how to qualify a software/font as Free(dom) .


### Nov 1st :
- **Venue** (yet to be finalized).

  - GEC and BGVS have come forward to ask permission in their colleges for the same.

  - GEC  - Amit, Sumanth and Darshan

  - BGVS  - voidspacexyz, BGVS team



- **Things Needed @ Venue**

  - Mic and Projector

  - Power supply to accomadate a decent number of laptops

  - Internet access ( possibly)


**Note** : We are not sure of the participant count. It could just be that nobody turns up and just too many people come in. So the venue needs to be flexiable and be able to co-operate. Tough, the internet could be a little compromise we could make.


- **Final Speakers** (Yet to be finalized)

  - Mamtha/Madhu from ALF (Mamtha is a kannada writer and poet.)      - Yet to approach. - voidspacexyz

  - Two Kannada localisation contributors (Omshiv prakash, Prabodh and Yogi are in the list so far.)                        - Yet to approach. - voidspacexyz, Darshan, Madhu & anybody interested

  - One Kannada writer and Poet                                       - Yet to approach. - voidspacexyz & BGVS Hassan Team.



**Note** : The aim of the speakers is to share the importance of a language and how it helps local communities grow. It is also important that the speakers understand the importance and need for FOSS here in the context and make relevant relations for language and FOSS. Speakers needs to clearly told the philosophies of FOSS before going on stage. Else, no matter what, we are dropping the speaker, if the organizers feel the speaker dosent have the right understanding of FOSS. While approaching speakers, this is an important point to be kept in mind.

**PS** : There are other places also that we could approach. But we are restricting ourselves , for we dont want to be hit by the number of volunteers that would be available there.


## Translations Planned (Core- Team to decide this)


#### GLUG :
    - Articles that explain various sectors of FOSS (https://etherpad.fsmk.org/p/r.52c8d813bee6d7a10c55f9f00fbe5f84)
    - WikiPedia articles
    - WikiPedia image translations
    - OSM transliterations and verifying the same

#### Schools :
    - WikiPedia Articles
    - Bag of Words
    - Wiktionary
    - FSMK website remaining translations (FSMK to give us parts that are not translated)

#### Public :

    - WikiPedia Diagrams (like body parts etc)
    - Wiktionary
    - Kannada words on various FOSS tools ( Needs a lot of work to collect all the un-translated words). Refer Software Section for the list of softwares.




## Softwares Possible :    
    - GIMP
    - Inkscape
    - Celestia
    - Stellarium
    - GNU Cash
    - GTK-Record my desktop
    - GParted
    - JOSM
    - Stellarium
    - Syncthing
    - Zim Desktop Wiki
    - XFCE Desktop
    - LXDE Desktop
    - Cinnamon Desktop
    - Wikipedia articles (Target is  about 20 articles)
    - Verifying OSM transliterations
    - Translating some of the FOSS related documentaries.

## Reading Section (WIP)

    - What is free and open source software

    - https://www.gnu.org/philosophy/free-sw.html

    - https://www.gnu.org/proprietary/proprietary.html

    - https://www.gnu.org/philosophy/open-source-misses-the-point.html

    - https://www.gnu.org/philosophy/free-software-even-more-important.html

    - What is localisation

    - https://www.w3.org/International/questions/qa-i18n

    - What is wikipedia and Free Knowledge

    - Open Data and its need

    - Map Data and its importance, and its relavance to OSM


## Translation Setup and Understanding (Links will be updated soon):

    - Difference between translation and transliteration

    - Zanata Docs on translation

    - http://docs.zanata.org/en/release/

    - Wikipedia docs on translation

    - OSM (OpenStreet Maps) docs on transliteration

    - Installing Language Fonts and KeyBoard in both Ubuntu and Fedora

## Pending Tasks
  - Posters for the event  - No one yet.
  - A web dashboard page to show our target and what we achieved on Nov 1 and throughout.   - Rizma

**Note**:
  - We are doing our best to keep less bloat from Bangalore, since we want the local team there to self run and maintain their activities going further. This is one of the most important part of any GLUG. This is a huge step taken up by Hassan team.
  Current Bangalore team size visiting Hassan is :  **5**


Uncategrized Links :

        - https://sanchaya.org/kannada-l10n/
